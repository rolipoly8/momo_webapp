<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\MomoRepository;
use App\Http\Controllers\Controller;
use App\Models\Momo;
use App\Models\Transaction;
use App\Models\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class MomoController extends Controller
{
    protected $momoRepository;

    public function __construct(MomoRepository $momoRepository)
    {
        $this->momoRepository = $momoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $momos = $this->momoRepository->all();
        return $momos;
        die('1');
    }

    /*
     * Thêm tài khoản mới
     * */
    public function create_momo(Request $request)
    {
        $request = json_decode($request->getContent());

        try {
            $account = $request->account;
            if (!$account) {
                throw new \Exception('Account cannot empty');
            }
            $momoInfo = $this->momoRepository->find_by(['phone' => $account]);
            if ($momoInfo) {
               return response(['status' => 1, 'message' => 'Updated Successfull']);
            }
            $status = 0;
            $name = $request->name;
            $data = [
                'phone' => $account,
                'status' => $status,
                'name' => $name,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->momoRepository->insert($data);
            return response(['status' => 1, 'message' => 'Updated Successfull']);
        } catch (\Exception $e) {
            return response(['status' => 2, 'message' => $e->getMessage()]);
        }
    }

    /*
     * Cập nhật trạng thái tài khoản
     * */
    public function update_status(Request $request)
    {
        try {
            $request = json_decode($request->getContent());
            $account = $request->account;
            if (!$account) {
                throw new \Exception('Account cannot empty');
            }
            $momo = $this->momoRepository->find_by(['phone' => $account]);
            if (!$momo) {
                throw new \Exception('Account not found');
            }
            $status = $request->status;
            if ($status == 'true') {
                $status = 1;
            } else {
                $status = 0;
            }
            $data = [
                'phone' => $account,
                'status' => $status,
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            Momo::query()->where(['id' => $momo->id])->update($data);
            return response(['status' => 1, 'message' => 'Updated Successfull']);
        } catch (\Exception $e) {
            return response(['status' => 2, 'message' => $e->getMessage()]);
        }
    }

    /*
     * Nhận giao dịch
     * */
    public function push_charge(Request $request)
    {
        try {
            $request = json_decode($request->getContent());

            $phone_sender = $request->phone_number;
            $transaction_id = isset($request->transaction_id) ? $request->transaction_id : null;
            $phone_receive = $request->account;
            $amount = $request->amount;
            $body = $request->body;
            $balance = $request->blance;
            $momo = $this->momoRepository->find_by(['phone' => $phone_receive]);
            //   return json_encode($momo);
            if (!$momo) {
                throw new \Exception('Account not found');
            }
            if (!isset($transaction_id)) {
                throw new \Exception('Transaction not found');
            }
            $transaction = Transaction::query()->where(['transaction_id'=>$transaction_id])->first();

            if ($transaction) {
                throw new \Exception('Transaction already exists');
            }

            $data = [
                'phone_receive' => $phone_receive,
                'phone_sender' => $phone_sender,
                'amount' => $amount,
                'content' => $body,
                'balance' => $balance,
                'momo_id' => $momo->id,
                'transaction_id' => $transaction_id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ];
            $transactionId = Transaction::query()->insertGetId($data);
            $this->sendCallBack($transactionId);
            return response(['status' => 1, 'message' => 'Successfull']);

        } catch (\Exception $e) {
            return response(['status' => 2, 'message' => $e->getMessage()]);
        }
    }

    /*
     * data: {
      "account": "0349603189",
      "phone_number": "0987502408",
      "status": "success",
      "body": "",
      "amount": "1000",
      "blance": 12787
    }
    */
    public function push_log(Request $request)
    {
        try {
            $request = json_decode($request->getContent());

            $phone_receive = $request->phone_number;
            $phone_sender = $request->account;
            $amount = $request->amount;
            $body = $request->body;
            $balance = isset($request->blance) ? $request->blance : null;
            $charge_id = isset($request->charge_id) ? $request->charge_id : null;
            $status = isset($request->status) ? $request->status : null;

            //if (!$charge_id) throw new \Exception('charge_id not found');
            if (!$balance) throw new \Exception('blance not found');
            
            $data = [
                'phone_receive' => $phone_receive,
                'phone_sender' => $phone_sender,
                'amount' => $amount,
                'content' => $body,
                'balance' => $balance,
             //   'charge_id' => $charge_id,
                'status' => $status,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),

            ];
            Withdraw::query()->insertGetId($data);

            return response(['status' => 1, 'message' => 'Successfull']);

        } catch (\Exception $e) {
            return response(['status' => 2, 'message' => $e->getMessage()]);
        }
    }

    public function sendCallBack($transID)
    {
        $transaction = Transaction::query()->where(['id' => $transID])->first();
        Http::post('https://webhook.site/554499fa-43ff-4e32-9a9e-b661bba713d6', json_decode($transaction, true));
    }
}
