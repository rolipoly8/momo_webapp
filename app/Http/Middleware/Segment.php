<?php

namespace App\Http\Middleware;
use http\Env\Request;
use Closure;

class Segment
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->isMethod('post')) {
            return response(json_encode(['status' => -1, 'message' => $request->segment(2)]));
        }
        return $next($request);

    }
}
