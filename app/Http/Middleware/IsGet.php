<?php

namespace App\Http\Middleware;
use http\Env\Request;
use Closure;

class IsGet
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!$request->isMethod('get')) {
            return response(json_encode(['status' => -1, 'message' => 'Method is not support']));
        }
        return $next($request);

    }
}
