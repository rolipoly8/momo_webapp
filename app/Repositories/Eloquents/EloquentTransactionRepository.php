<?php

namespace App\Repositories\Eloquents;

use App\Contracts\Repositories\TransactionRepository;
use App\Models\Momo;

class EloquentTransactionRepository extends EloquentBaseRepository implements TransactionRepository
{
    protected $model;

    public function __construct(Momo $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->get();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function insert($data)
    {
        // TODO: Implement insert() method.
        return $this->model->insertGetId($data);
    }
}
