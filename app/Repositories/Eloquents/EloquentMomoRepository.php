<?php

namespace App\Repositories\Eloquents;

use App\Contracts\Repositories\MomoRepository;
use App\Models\Momo;

class EloquentMomoRepository extends EloquentBaseRepository implements MomoRepository
{
    protected $model;

    public function __construct(Momo $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->get();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function insert($data)
    {
        // TODO: Implement insert() method.
        return $this->model->insertGetId($data);
    }
    public function find_by($condition)
    {
        // TODO: Implement find_by() method.
        return $this->model->where($condition)->first();
    }
}
