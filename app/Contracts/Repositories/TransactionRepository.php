<?php

namespace App\Contracts\Repositories;

interface TransactionRepository extends BaseRepository
{
    public function all();
    //   public function paginate($items = null);
    public function find($id);
    public function insert($data);
}