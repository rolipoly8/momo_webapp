<?php
namespace App\Contracts\Repositories;

interface MomoRepository extends BaseRepository
{
    public function all();

    //   public function paginate($items = null);
    public function find($id);

    public function insert($data);
    public function find_by($condition);

}