<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(
            'App\Contracts\Repositories\UserRepository',
            'App\Repositories\Eloquents\EloquentUserRepository'
        );
        $this->app->bind(
            'App\Contracts\Repositories\MomoRepository',
            'App\Repositories\Eloquents\EloquentMomoRepository'
        );
        $this->app->bind(
            'App\Contracts\Repositories\TransactionRepository',
            'App\Repositories\Eloquents\EloquentTransactionRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
