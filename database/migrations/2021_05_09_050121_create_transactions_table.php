<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('phone_receive')->nullable();
            $table->string('phone_sender')->nullable();
            $table->bigInteger('amount')->default(0);
            $table->string('content')->nullable();
            $table->bigInteger('balance')->nullable();
            $table->unsignedBigInteger('momo_id')->index()->nullable();
            $table->unsignedBigInteger('user_id')->index()->nullable();
            $table->foreign('momo_id')->references('id')->on('momos');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
