@extends('layouts.midone')

@section('content')

<div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
                        <table class="table table-report -mt-2">
                            <thead>
                                <tr>
                                    <th class="whitespace-no-wrap">SỐ NHẬN</th>
                                    <th class="whitespace-no-wrap">SỐ GỬI</th>
                                    <th class="text-right whitespace-no-wrap">SỐ TIỀN</th>
                                    <th class="text-right whitespace-no-wrap">NỘI DUNG</th>
                                    <th class="text-center whitespace-no-wrap">TRẠNG THÁI</th>
                                    <th class="text-center whitespace-no-wrap">THỜI GIAN</th>
                                    <th class="text-center whitespace-no-wrap"></th>
                                </tr>
                            </thead>
                            <tbody>
							<?php
							foreach($transaction as $item){
							?>
                                <tr class="intro-x">
                                    <td class="w-40">
                                        <b>
                                            <?php echo $item['phone_sender']?>
                                        </b>
                                    </td>
                                    <td>
                                       <b><?php echo $item['phone_receive']?></b>
                                    </td>
                                   
                                    <td class="text-right"><?php echo number_format($item['amount'])?></td>
									 <td class=""><?php echo ($item['content'])?></td>
                                    <td class="w-40">
                                        <div class="flex items-center justify-center text-theme-9"> <i data-feather="check-square" class="w-4 h-4 mr-2"></i> Thành công </div>
                                    </td>
									<td><div class="flex"><?php echo date('H:i:s d/m/Y',strtotime($item['updated_at']));?></div></td>
                                    <td class="table-report__action w-56">
                                        <div class="flex justify-center items-center">
                                           <!-- <a class="flex items-center mr-3" href="javascript:;"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                                            <a class="flex items-center text-theme-6" href="javascript:;" data-toggle="modal" data-target="#delete-confirmation-modal"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </a> -->
                                        </div>
                                    </td>
                                </tr>
							<?php }?>
                            </tbody>
                        </table>
                    </div>

@endsection