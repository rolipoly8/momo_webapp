<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('')->group(function () {
    Route::get('/q', 'Api\MomoController@index');
    Route::get('/update_status', 'Api\MomoController@update_status')->middleware('valid.ispost');
    Route::post('/update_status', 'Api\MomoController@update_status')->middleware('valid.ispost');

    Route::get('/push_charge', 'Api\MomoController@push_charge')->middleware('valid.ispost');
    Route::post('/push_charge', 'Api\MomoController@push_charge')->middleware('valid.ispost');

    Route::post('/create_momo', 'Api\MomoController@create_momo')->middleware('valid.ispost');
    Route::get('/create_momo', 'Api\MomoController@create_momo')->middleware('valid.ispost');

    Route::post('/push_log', 'Api\MomoController@push_log')->middleware('valid.ispost');
    Route::get('/push_log', 'Api\MomoController@push_log')->middleware('valid.ispost');

});
Route::fallback(function(){
    return response()->json(['message' => 'API not found'], 404);
});